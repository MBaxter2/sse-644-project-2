"""
image_magics is a module of magic commands for IPython for displaying iamges
into a Jupyter Notebook instance.
"""
import os
from IPython.core.magic import (Magics, magics_class, line_magic, cell_magic)
from IPython.display import HTML
from traitlets import Bool

@magics_class
class ImageMagics(Magics):
    """ImageMagic class that allows for configuration of image_magics"""
    show_name = Bool(True, config=True)

    @line_magic
    def print_pic(self, line):
        """IPython magic command for loading a single picture in HTML"""
        if self.show_name:
            file = os.path.basename(line).split('.')[0]
            return HTML('<h1>'+file+'</h1><img src="'+line+'">')

        return HTML('<img src="'+line+'">')

    @cell_magic
    def print_pics(self, line, cell):
        """IPython magic cell for loading multiple pictures in an HTML table"""
        pics = cell.splitlines()
        htmlstr = '<table><tr>'
        i = 0
        for pic in pics:
            if i >= int(line):
                htmlstr += '</tr><tr>'
                i = 0
            else:
                i += 1
            if self.show_name:
                file = os.path.basename(pic).split('.')[0]
                htmlstr += '<td><hr>'+file+'</hr><img src="'+pic+'"></td>'
            else:
                htmlstr += '<td><img src="'+pic+'"></td>'
        htmlstr += '</tr></table>'
        return HTML(htmlstr)

def load_ipython_extension(ipython):
    """Load the extensions into ipython"""
    ipython.register_magics(ImageMagics)
