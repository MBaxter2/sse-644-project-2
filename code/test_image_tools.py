"""

"""
import unittest
import numpy as np
import matplotlib.image as mpimg
import image_tools as it
import os

class TestImageMethods(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._test_image = mpimg.imread('../data/test/image1.png')  
        cls._test_image2 = mpimg.imread('../data/test/image2.jpg')        
    
    def test_rgb_to_hsv(self):
        expected_hsv = np.loadtxt('../data/test/ex_hsv.txt').reshape((3,3,3))
        hsv_image = it.rgb_to_hsv(self._test_image)
        result = np.array_equal(hsv_image, expected_hsv)
        self.assertTrue(result)
        
    def test_hsv_to_rgb(self):   
        expected_rgb = np.loadtxt('../data/test/ex_rgb.txt').reshape((3,3,3)) 
        rgb_image = it.hsv_to_rgb(it.rgb_to_hsv(self._test_image))  
        result = np.array_equal(rgb_image, expected_rgb)  
        self.assertTrue(result)
    
    def test_change_hue1(self):       
        expected_hue = np.loadtxt('../data/test/ex_hue1.txt').reshape((3,3,3))
        hsv_image = it.rgb_to_hsv(self._test_image)
        hue_image = it.change_hue(hsv_image, 0)
        result = np.array_equal(hue_image, expected_hue)  
        self.assertTrue(result)
    
    def test_change_hue2(self):       
        expected_hue = np.loadtxt('../data/test/ex_hue2.txt').reshape((3,3,3))
        hsv_image = it.rgb_to_hsv(self._test_image)
        hue_image = it.change_hue(hsv_image, 0.5)
        result = np.array_equal(hue_image, expected_hue)  
        self.assertTrue(result)
    
    def test_change_hue3(self):       
        expected_hue = np.loadtxt('../data/test/ex_hue3.txt').reshape((3,3,3))
        hsv_image = it.rgb_to_hsv(self._test_image)
        hue_image = it.change_hue(hsv_image, -0.5)
        result = np.array_equal(hue_image, expected_hue)  
        self.assertTrue(result)
        
    def test_export_color_channels(self):
        r_file = '../data/processed/image_red.png'
        g_file = '../data/processed/image_green.png'
        b_file = '../data/processed/image_blue.png'
        
        if os.path.isfile(r_file):
            os.remove(r_file)
            
        if os.path.isfile(g_file):
            os.remove(g_file)
        
        if os.path.isfile(b_file):
            os.remove(b_file)
        
        it.export_color_channels(self._test_image)
        
        r, g, b = np.dsplit(self._test_image, 3)
        red = np.repeat(r, 3, axis=2)
        green = np.repeat(g, 3, axis=2)
        blue = np.repeat(b, 3, axis=2)
        
        red_image = mpimg.imread(r_file)
        green_image = mpimg.imread(g_file)
        blue_image = mpimg.imread(b_file)
        
        # Ignore alpha channel that is saved
        red_result = np.array_equal(red, red_image[:,:,0:3])
        green_result = np.array_equal(green, green_image[:,:,0:3])
        blue_result = np.array_equal(blue, blue_image[:,:,0:3])
        
        self.assertTrue(red_result)
        self.assertTrue(green_result)
        self.assertTrue(blue_result)      
        
    def test_value_threshold1(self):
        expected = np.loadtxt('../data/test/ex_thres1.txt').reshape((3,3,3))
        hsv_image = it.rgb_to_hsv(self._test_image)
        thres = it.value_threshold(hsv_image, 0.5, True)
        result = np.array_equal(thres, expected)
        self.assertTrue(result)
        
    def test_value_threshold2(self):
        expected = np.loadtxt('../data/test/ex_thres2.txt').reshape((3,3,3))
        hsv_image = it.rgb_to_hsv(self._test_image)
        thres = it.value_threshold(hsv_image, 0.5, False)
        result = np.array_equal(thres, expected)
        self.assertTrue(result)     
        
    def test_conv_image(self):
        expected = np.loadtxt('../data/test/ex_conv.txt').reshape((160,127,3))
        image = self._test_image2.astype(np.float) / 255
        conv_filter = np.array([[-1, -1, -1],[-1, 8, -1],[-1,-1,-1]])
        output = it.convolve_image(image, conv_filter)
        result = np.array_equal(expected, output)
        self.assertTrue(result)
        
    def test_grayscale(self):
        expected = np.loadtxt('../data/test/ex_gray.txt').reshape((3,3,3))
        gray = it.grayscale(self._test_image)
        result = np.array_equal(gray, expected)
        self.assertTrue(result)        
        
if __name__ == '__main__':
    unittest.main()
    
    