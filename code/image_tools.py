"""
image_tools is a module for manipulating images
"""
import colorsys as cs
import numpy as np
import matplotlib.image as mpimg

def rgb_to_hsv(image):
    """Converts an image from RGB color space into HSV

    Arguments:
        * image: NumPy 3D array
    Returns:
        * hsv: Original image converted to HSV
    """
    height, width, _ = image.shape
    hsv = np.zeros(image.shape)
    for i in range(height):
        for j in range(width):
            red, green, blue = image[i, j, ]
            hue, sat, val = cs.rgb_to_hsv(red, green, blue)
            hsv[i, j, ] = hue, sat, val
    return hsv

def hsv_to_rgb(image):
    """Converts an image from HSV into RGB color space

    Arguments:
        * image: NumPy 3D array
    Returns:
        * rgb: Original image converted to RGB
    """
    height, width, _ = image.shape
    rgb = np.zeros(image.shape)
    for i in range(height):
        for j in range(width):
            hue, sat, val = image[i, j, ]
            red, green, blue = cs.hsv_to_rgb(hue, sat, val)
            rgb[i, j, ] = red, green, blue
    return rgb

def change_hue(image, shift):
    """Shifts the hue of an image. The image must be in HSV format.

    Arguments:
        * image: NumPy 3D array
        * shift: The hue shift value
    Returns:
        * adjusted: Hue shifted image
    """
    height, width, _ = image.shape
    shifted = np.zeros(image.shape)
    for i in range(height):
        for j in range(width):
            hue = image[i, j, 0]
            hue = (hue + shift) % 1
            shifted[i, j, 0] = hue
            shifted[i, j, 1] = image[i, j, 1]
            shifted[i, j, 2] = image[i, j, 2]
    return shifted

def export_color_channels(image):
    """Saves the different color channels of image to three files
    '../data/processed/image_red.png' for the red channel
    '../data/processed/image_green.png' for the green channel
    '../data/processed/image_blue.png' for the blue channel

    Arguments:
        * image: NumPy 3D array
    Returns:
        * none
    """
    red, green, blue = np.dsplit(image, 3)
    red_image = np.repeat(red, 3, axis=2)
    green_image = np.repeat(green, 3, axis=2)
    blue_image = np.repeat(blue, 3, axis=2)
    mpimg.imsave('../data/processed/channel_red.png', red_image)
    mpimg.imsave('../data/processed/channel_green.png', green_image)
    mpimg.imsave('../data/processed/channel_blue.png', blue_image)

def value_threshold(image, threshold, invert):
    """Coverts image to black and white. The image must be in HSV format

    Arguments:
        * image: NumPy 3D array
        * threshold: The threshold value
        * invert: Boolean to swap black and white
    Returns:
        * adjusted: Threshold image
    """
    height, width, _ = image.shape
    adjusted = np.zeros(image.shape)
    for i in range(height):
        for j in range(width):
            if image[i, j, 2] > threshold:
                adjusted[i, j, 2] = 0 if invert else 1
            else:
                adjusted[i, j, 2] = 1 if invert else 0
    return adjusted

def __convolve_patch(image, conv_filter, pos, output):
    """Convolves the filter over a single position of the image

    Arguments:
        * image: NumPy 3D array
        * conv_filter: Convolution filter
        * pos: Tuple of the image position and filter offsets
        * output: NumPy 3D which the convolution results are written to
    Returns:
        * output: image processed with the convolution filter
    """
    filter_height, filter_width = conv_filter.shape
    i, j, offset_h, offset_w = pos

    for fil_i in range(filter_height):
        imag_i = i + fil_i - offset_h
        for fil_j in range(filter_width):
            imag_j = j - fil_j - offset_w
            output[i, j, 0] += image[imag_i, imag_j, 0] * conv_filter[fil_i, fil_j]
            output[i, j, 1] += image[imag_i, imag_j, 1] * conv_filter[fil_i, fil_j]
            output[i, j, 2] += image[imag_i, imag_j, 2] * conv_filter[fil_i, fil_j]

    output[i, j, 0] = max(0, min(1, output[i, j, 0]))
    output[i, j, 1] = max(0, min(1, output[i, j, 1]))
    output[i, j, 2] = max(0, min(1, output[i, j, 2]))

def convolve_image(image, conv_filter):
    """Convolves the image with the filter

    Arguments:
        * image: NumPy 3D array
        * conv_filter: Convolution filter
    Returns:
        * output: image processed with the convolution filter
    """
    image_height, image_width, _ = image.shape
    filter_height, filter_width = conv_filter.shape

    output = np.zeros(image.shape)
    offset_h = (filter_height - 1) // 2
    offset_w = (filter_width - 1) // 2

    for i in range(offset_h, image_height - offset_h):
        for j in range(offset_w, image_width - offset_w):
            pos = (i, j, offset_h, offset_w)
            __convolve_patch(image, conv_filter, pos, output)

    return output

def grayscale(image):
    """Coverts image to gray scale. The image must be in RGB format.

    Arguments:
        * image: NumPy 3D array
    Returns:
        * gray scaled image
    """
    red, green, blue = np.dsplit(image, 3)
    gray = 0.21 * red + 0.72 * green + 0.07 * blue
    return np.dstack((gray, gray, gray))
    